package Modul3C;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Modul3C_nomor1 extends JFrame {

    private static final int FRAME_WIDTH = 700;
    private static final int FRAME_HEIGHT = 500;

    private JMenuBar menuUtama;
    private JMenu menu_File;
    private JMenu menu_Edit;
    private JMenuItem tampil1;
    private JMenuItem tampil2;

    public static void main(String[] args) {
        Modul3C_nomor1 frame = new Modul3C_nomor1();
        frame.setVisible(true);
    }

    public Modul3C_nomor1() {
        this.setTitle("Frame Pertama");

        menuUtama = new JMenuBar();
        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");

        menuUtama.add(menu_File);
        menuUtama.add(menu_Edit);
        this.setJMenuBar(menuUtama);

        tampil1 = new JMenuItem("Tampil 1");
        tampil2 = new JMenuItem("Tampil 2");
        menu_File.add(tampil1);
        menu_File.add(tampil2);

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

}
