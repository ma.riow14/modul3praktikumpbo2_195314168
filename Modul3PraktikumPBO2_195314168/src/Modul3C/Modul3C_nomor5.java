package Modul3C;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class Modul3C_nomor5 extends JDialog {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 500;

    private JTextArea text1;
    private JButton button_left;
    private JButton button_right;
    private JCheckBox check1;
    private JCheckBox check2;
    private JCheckBox check3;
    private JRadioButton opsi1;
    private JRadioButton opsi2;
    private JRadioButton opsi3;

    public static void main(String[] args) {
        Modul3C_nomor5 dialog = new Modul3C_nomor5();
        dialog.setVisible(true);
    }

    public Modul3C_nomor5() {

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setTitle("Check Box Demo");

        Container contentPane;
        contentPane = getContentPane();
        contentPane.setLayout(null);

        text1 = new JTextArea("Welcome To Java");
        text1.setBounds(160, 150, 100, 40);
        contentPane.add(text1);

        button_left = new JButton("Left");
        button_left.setBounds(100, 240, 80, 40);
        contentPane.add(button_left);

        button_right = new JButton("Right");
        button_right.setBounds(250, 240, 80, 40);
        contentPane.add(button_right);

        check1 = new JCheckBox("Centered");
        check1.setBounds(350, 80, 80, 50);
        contentPane.add(check1);

        check2 = new JCheckBox("Bold");
        check2.setBounds(350, 130, 80, 50);
        contentPane.add(check2);

        check3 = new JCheckBox("Italic");
        check3.setBounds(350, 180, 80, 50);
        contentPane.add(check3);

        opsi1 = new JRadioButton("Red");
        opsi1.setBounds(10, 90, 60, 50);
        contentPane.add(opsi1);

        opsi2 = new JRadioButton("Green");
        opsi2.setBounds(10, 131, 60, 50);
        contentPane.add(opsi2);

        opsi3 = new JRadioButton("Blue");
        opsi3.setBounds(10, 180, 60, 50);
        contentPane.add(opsi3);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
