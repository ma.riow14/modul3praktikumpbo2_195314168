package Modul3C;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class Modul3C_nomor2 extends JDialog {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 500;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;

    public static void main(String[] args) {
        Modul3C_nomor2 dialog = new Modul3C_nomor2();
        dialog.setVisible(true);
    }

    public Modul3C_nomor2() {
        Container contentPane;
        JButton yellow, blue, red;

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setTitle("Button Test");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentPane = getContentPane();
        contentPane.setBackground(Color.white);
        contentPane.setLayout(new GridLayout(1, 3));
        contentPane.setLayout(null);

        yellow = new JButton("Yellow");
        blue = new JButton("Blue");
        red = new JButton("Red");

        yellow.setBounds(110, 60, 80, 40);
        blue.setBounds(205, 60, 80, 40);
        red.setBounds(305, 60, 80, 40);

        contentPane.add(yellow);
        contentPane.add(blue);
        contentPane.add(red);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
