package Modul3C_nomor6;

import java.awt.Container;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import java.lang.String;
import javafx.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;

public class Modul3C_nomor6 extends JDialog {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 500;

    private JLabel label_negara;
    private JTextArea kolom_negara;
    private JComboBox pilihan_negara;
    private JScrollPane scroll;

    public static void main(String[] args) {
        Modul3C_nomor6 dialog = new Modul3C_nomor6();
        dialog.setVisible(true);
    }

    public Modul3C_nomor6() {

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setTitle("ComboBoxDemo");

        Container contentPane;
        contentPane = getContentPane();
        contentPane.setLayout(null);

        String negara[] = {"Kanada", "Indoesia", "Inggris", "Malaysia"};

        pilihan_negara = new JComboBox(negara);
        pilihan_negara.setBounds(0, 0, 475, 50);
        contentPane.add(pilihan_negara);

        ImageIcon gambar = new ImageIcon(getClass().getResource("kanada 2.jpg"));
        label_negara = new JLabel();
        label_negara.setIcon(gambar);
        label_negara.setBounds(0, 100, 200, 100);
        contentPane.add(label_negara);

        JTextArea kolom_negara = new JTextArea("Negara Canada");
        kolom_negara.setBounds(300, 200, 100, 100);
        JScrollPane scroll = new JScrollPane(kolom_negara, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setBounds(300, 50, 160, 200);
        contentPane.add(scroll);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }
}
