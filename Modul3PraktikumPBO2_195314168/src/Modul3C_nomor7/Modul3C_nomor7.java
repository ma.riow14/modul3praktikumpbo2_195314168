package Modul3C_nomor7;

import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Modul3C_nomor7 extends JDialog {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 500;

    private JLabel label_negara;
    private JScrollPane scroll;
    private JList list;

    public static void main(String[] args) {
        Modul3C_nomor7 dialog = new Modul3C_nomor7();
        dialog.setVisible(true);
    }

    public Modul3C_nomor7() {
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setTitle("ComboBoxDemo");

        Container contentPane;
        contentPane = getContentPane();
        contentPane.setLayout(null);

        String negara[] = {"Canada", "China", "Denmark", "France", "Germany",
            "India", "Norway", "United Kingdom"};

        JPanel listPanel = new JPanel(new GridLayout(0, 1));
        list = new JList(negara);
        list.setBounds(0, 0, 100, 150);
        listPanel.add(new JScrollPane(list));
        contentPane.add(list);

        ImageIcon gambar1 = new ImageIcon(getClass().getResource("kanada 2.jpg"));
        label_negara = new JLabel();
        label_negara.setIcon(gambar1);
        label_negara.setBounds(130, 10, 100, 100);
        contentPane.add(label_negara);

        ImageIcon gambar2 = new ImageIcon(getClass().getResource("indonesia 2.gif"));
        label_negara = new JLabel();
        label_negara.setIcon(gambar2);
        label_negara.setBounds(250, 10, 100, 100);
        contentPane.add(label_negara);

        ImageIcon gambar3 = new ImageIcon(getClass().getResource("jerman -1.png"));
        label_negara = new JLabel();
        label_negara.setIcon(gambar3);
        label_negara.setBounds(370, 10, 100, 100);
        contentPane.add(label_negara);

        ImageIcon gambar4 = new ImageIcon(getClass().getResource("india -1.jpg"));
        label_negara = new JLabel();
        label_negara.setIcon(gambar4);
        label_negara.setBounds(130, 100, 100, 100);
        contentPane.add(label_negara);

        ImageIcon gambar5 = new ImageIcon(getClass().getResource("inggris 2.jpg"));
        label_negara = new JLabel();
        label_negara.setIcon(gambar5);
        label_negara.setBounds(250, 100, 100, 100);
        contentPane.add(label_negara);

        ImageIcon gambar6 = new ImageIcon(getClass().getResource("amerika -1.jpg"));
        label_negara = new JLabel();
        label_negara.setIcon(gambar6);
        label_negara.setBounds(370, 100, 100, 100);
        contentPane.add(label_negara);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
