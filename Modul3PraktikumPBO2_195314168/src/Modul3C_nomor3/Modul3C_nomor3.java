package Modul3C_nomor3;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import java.awt.Image;
import javax.swing.*;

public class Modul3C_nomor3 extends JDialog {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 300;

    private JLabel label_text;
    private JLabel label_image;

    public static void main(String[] args) {
        Modul3C_nomor3 dialog = new Modul3C_nomor3();
        dialog.setVisible(true);
    }

    public Modul3C_nomor3() {

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setTitle("Text and Icon Label");
        this.setLayout(null);

        ImageIcon gambar = new ImageIcon(getClass().getResource("grapes 2.jpg"));
        label_image = new JLabel();
        label_image.setIcon(gambar);
        label_image.setBounds(200, 20, 100, 100);
        this.add(label_image);

        label_text = new JLabel("Grapes");
        label_text.setBounds(217, 120, 80, 40);
        this.add(label_text);
        
         setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }

}
